from discord_webhook import DiscordWebhook, DiscordEmbed
from datetime import datetime
from webhook import *
import time
import requests
import json
import pytz

utc_tz = pytz.timezone('UTC')
cest_tz = pytz.timezone('Europe/Berlin')

url = 'https://politiloggen-vis-frontend.bks-prod.politiet.no/api/messagethread'

headers = {
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate, br',
    'Content-Type': 'application/json; charset=UTF-8'
}

data = {
    "sortByEnum": "Date",
    "sortByAsc": False,
    "timeSpanType": "Custom",
    "dateTimeFrom": "2022-01-01T00:00:00.000Z",
    "dateTimeTo": "2024-05-30T13:20:53.184Z",
    "skip": 0,
    "take": 1000,
    "category": []
}

def post_to_discord(message):
    print(message)
    #print(message["category"])
    webhook = DiscordWebhook(url=webhook_url, username="Politiloggen")
    if message["category"] == "Ro og orden":
        embedColour = 0xffff00
    elif message["category"] == "Brann":
        embedColour = 0xff0000
    elif message["category"] == "Sjø":
        embedColour = 0x0000ff
    elif message["category"] == "Trafikk":
        embedColour = 0x00ffff
    elif message["category"] == "Tyveri":
        embedColour = 0x777777
    elif message["category"] == "Ulykke":
        embedColour = 0x777777
    else:
        embedColour = 0x50bdfe
    if message["area"] == '':
        embed = DiscordEmbed(title=message["category"], description=message["municipality"], color=embedColour)
    else:
        embed = DiscordEmbed(title=message["category"], description=f'{message["area"]}, {message["municipality"]}', color=embedColour)
    timeStamp = datetime.fromisoformat(message["messages"][0]["createdOn"])
    dt_utc = timeStamp.astimezone(utc_tz)
    dt_cest = dt_utc.astimezone(cest_tz)
#    print(timeStamp)
    embed.set_author(name=message["district"], url=f'https://www.politiet.no/politiloggen/hendelse/#/{message["id"]}/')#, icon_url="author icon url")
    embed.add_embed_field(name=dt_cest.strftime("%A, %B %d, %Y at %H:%M:%S %Z"), value=message["messages"][0]["text"])
#    embed.set_footer(text=timeStamp)
    webhook.add_embed(embed)
    #response = webhook.execute()

response = requests.post(url, headers=headers, data=json.dumps(data))

data = json.loads(response.text)

for n in json.loads(response.text)['messageThreads']:
    post_to_discord(n)
